package com.muni.analyzer.data.model.json

import com.muni.analyzer.data.model.json.FinalResult.ProtocolReport

final case class FinalResult(
    macAddress: String,
    newProtocols: Map[String, ProtocolReport],
    oldSuspiciousProtocols: Map[String, ProtocolReport],
    possibleInfections: Set[String],
    blacklistedDomains: Set[String])

object FinalResult {
  final case class ProtocolReport(defaultPorts: Set[Int],
                                  flowStatistics: NetFlowStatistics,
                                  knownIssue: Boolean,
                                  domainStatistics: List[DomainStatistics])
}
