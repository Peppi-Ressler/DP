package com.muni.analyzer.data.model.json

import com.muni.analyzer.data.entity.{
  DeviceFingerprintEntity,
  DomainStatisticsEntity,
  ProtocolStatisticsEntity
}
import com.muni.analyzer.data.model.json.NetFlowStatistics.FlowStats

final case class DeviceFingerprint(
    deviceIp: String,
    macAddress: String,
    protocolStatistics: Map[String, ProtocolStatistics],
    uniqueDomains: Set[String])
object DeviceFingerprint {
  def apply(from: DeviceFingerprintEntity): DeviceFingerprint = {
    DeviceFingerprint(
      from.deviceIP,
      from.macAddress,
      from.protocolStatistic
        .mapValues(ProtocolStatistics(_))
        .map(pair => pair._1.replace("\u2024", ".") -> pair._2),
      from.uniqueDomains.to
    )
  }
}

final case class NetFlowStatistics(flowSize: Long,
                                   flowDuration: Float,
                                   packets: Int,
                                   flows: Int)
object NetFlowStatistics {
  type FlowStats = (Long, Float, Int, Int)
  def apply(from: FlowStats): NetFlowStatistics =
    NetFlowStatistics(from._1, from._2, from._3, from._4)

}

final case class ProtocolStatistics(defaultPorts: Set[Int],
                                    flowStatistics: NetFlowStatistics,
                                    misused: Boolean,
                                    domainStatistics: Seq[DomainStatistics],
                                    protocolUsage: ProtocolUsage)
object ProtocolStatistics {
  def apply(
      tuple: (Set[Int], NetFlowStatistics, Boolean, Seq[DomainStatistics]),
      protocolUsage: ProtocolUsage): ProtocolStatistics = {
    ProtocolStatistics(tuple._1, tuple._2, tuple._3, tuple._4, protocolUsage)
  }
  def apply(from: ProtocolStatisticsEntity): ProtocolStatistics = {
    ProtocolStatistics(from.defaultPorts.to,
                       from.flowStatistics,
                       from.misused,
                       from.domainStatistics.map(DomainStatistics.apply),
                       from.protocolUsage)
  }
}

final case class ProtocolUsage(averageHits: Double,
                               lowerBoundary: Float,
                               higherBoundary: Float,
                               hitsPerMinute: List[Double])
object ProtocolUsage {
  def empty: ProtocolUsage = ProtocolUsage(0d, 0f, 0f, List.empty)
}

final case class DomainStatistics(domainIp: String,
                                  netFlowStatistics: NetFlowStatistics,
                                  nonDefaultDstPorts: Set[Int],
                                  location: String)

object DomainStatistics {
  def apply(domainIp: String,
            flowStats: FlowStats,
            dstPort: Set[Int],
            country: String): DomainStatistics = {
    DomainStatistics(domainIp, NetFlowStatistics(flowStats), dstPort, country)
  }
  def apply(from: DomainStatisticsEntity): DomainStatistics = {
    DomainStatistics(from.domainIp,
                     from.netFlowStatistics,
                     from.nonDefaultDstPorts.getOrElse(Seq.empty).toSet,
                     from.location)
  }
}
