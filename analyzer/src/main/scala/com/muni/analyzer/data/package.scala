package com.muni.analyzer

import com.muni.analyzer.data.model.internal.Anomaly
import com.muni.analyzer.data.model.json.FinalResult.ProtocolReport
import com.muni.analyzer.data.model.json._
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

package object data {

  implicit val deviceStatisticsDecoder: Decoder[DeviceFingerprint] =
    deriveDecoder
  implicit val deviceStatisticsEncoder: Encoder[DeviceFingerprint] =
    deriveEncoder

  implicit val protocolStatisticsDecoder: Decoder[ProtocolStatistics] =
    deriveDecoder
  implicit val protocolStatisticsEncoder: Encoder[ProtocolStatistics] =
    deriveEncoder

  implicit val protocolUsageEncoder: Encoder[ProtocolUsage] = deriveEncoder
  implicit val protocolUsageDecoder: Decoder[ProtocolUsage] = deriveDecoder

  implicit val domainStatisticsDecoder: Decoder[DomainStatistics] =
    deriveDecoder
  implicit val domainStatisticsEncoder: Encoder[DomainStatistics] =
    deriveEncoder

  implicit val flowStatisticsDecoder: Decoder[NetFlowStatistics] = deriveDecoder
  implicit val flowStatisticsEncoder: Encoder[NetFlowStatistics] = deriveEncoder

  implicit val anomalyEncoder: Encoder[Anomaly] = deriveEncoder

  implicit val finalResultEncoder: Encoder[FinalResult] = deriveEncoder

  implicit val protocolReportEncoder: Encoder[ProtocolReport] = deriveEncoder
}
