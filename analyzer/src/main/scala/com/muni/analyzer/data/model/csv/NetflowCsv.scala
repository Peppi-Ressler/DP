package com.muni.analyzer.data.model.csv

final case class NetflowCsv(start_time: String,
                            flowDuration: Float,
                            protocol: String,
                            srcIp: String,
                            srcPort: Int,
                            dstIp: String,
                            dstPort: Int,
                            packets: Int,
                            flowSize: Long,
                            flows: Int,
                            resolvedIp: String,
                            macAddress: String)
