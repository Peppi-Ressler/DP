package com.muni.analyzer.data.model.internal

final case class DefaultServiceAndPort(serviceName: String,
                                       defaultPorts: Set[Int],
                                       knownIssue: Boolean,
                                       transportProtocols: Set[String])
