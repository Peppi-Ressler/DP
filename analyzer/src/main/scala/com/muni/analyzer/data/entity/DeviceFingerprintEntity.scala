package com.muni.analyzer.data.entity

import com.muni.analyzer.data.model.json._

final case class NetworkEntity(devices: Seq[DeviceFingerprintEntity],
                               router: DeviceFingerprintEntity)

final case class DeviceFingerprintEntity(
    deviceIP: String,
    macAddress: String,
    protocolStatistic: Map[String, ProtocolStatisticsEntity],
    uniqueDomains: Seq[String])

object DeviceFingerprintEntity {
  def apply(from: DeviceFingerprint): DeviceFingerprintEntity = {
    DeviceFingerprintEntity(
      from.deviceIp,
      from.macAddress,
      from.protocolStatistics
        .mapValues(ProtocolStatisticsEntity.apply)
        .map(pair => pair._1.replace(".", "\u2024") -> pair._2),
      from.uniqueDomains.to
    )
  }
}

final case class ProtocolStatisticsEntity(
    defaultPorts: List[Int],
    flowStatistics: NetFlowStatistics,
    misused: Boolean,
    domainStatistics: Seq[DomainStatisticsEntity],
    protocolUsage: ProtocolUsage)

object ProtocolStatisticsEntity {
  def apply(from: ProtocolStatistics): ProtocolStatisticsEntity = {
    ProtocolStatisticsEntity(
      from.defaultPorts.to,
      from.flowStatistics,
      from.misused,
      from.domainStatistics.map(DomainStatisticsEntity.apply),
      from.protocolUsage
    )
  }
}

final case class DomainStatisticsEntity(domainIp: String,
                                        netFlowStatistics: NetFlowStatistics,
                                        nonDefaultDstPorts: Option[Seq[Int]] =
                                          None,
                                        location: String)

object DomainStatisticsEntity {
  def apply(from: DomainStatistics): DomainStatisticsEntity = {
    DomainStatisticsEntity(
      from.domainIp,
      from.netFlowStatistics,
      if (from.nonDefaultDstPorts.nonEmpty) Some(from.nonDefaultDstPorts.to)
      else None,
      from.location
    )
  }
}
