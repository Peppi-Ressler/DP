package com.muni.analyzer.data.model.csv

final case class ServicePortCsv(serviceName: String,
                                port: String,
                                protocol: String,
                                knownUnauthorizedAccess: String)
