package com.muni.analyzer.data.dao

import com.muni.analyzer.data.entity.DeviceFingerprintEntity
import org.mongodb.scala.MongoDatabase

// ala repository pattern
class FingerprintRepository(mongoDB: MongoDatabase, collectionName: String)
    extends GenericTaskMongoDAO[DeviceFingerprintEntity](mongoDB,
                                                         collectionName)
