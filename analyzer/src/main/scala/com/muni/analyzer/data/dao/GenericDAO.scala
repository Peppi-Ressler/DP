package com.muni.analyzer.data.dao

import scala.language.higherKinds

trait GenericDAO[F[_], T] {
  def insert(document: T): F[Unit]

  def insertMany(documents: Seq[T]): F[Unit]

  def findAll(): F[Seq[T]]

  def findSpecific[Value](key: String, value: Value): F[Option[T]]

  def replace[Value](document: T, key: String, value: Value): F[Unit]

  def replaceAll[Value](documents: List[T], key: String)(
      filter: T => Value): F[Unit]
}
