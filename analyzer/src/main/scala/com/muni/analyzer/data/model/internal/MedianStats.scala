package com.muni.analyzer.data.model.internal

final case class MedianStats(median: Double, mad: Double)
