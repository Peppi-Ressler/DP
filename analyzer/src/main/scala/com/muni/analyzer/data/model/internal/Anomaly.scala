package com.muni.analyzer.data.model.internal

import com.muni.analyzer.data.model.json.DeviceFingerprint

final case class Anomaly(original: DeviceFingerprint,
                         infected: DeviceFingerprint,
                         similarity: Float,
                         zScore: Map[String, Double],
                         botnets: Map[String, String])
