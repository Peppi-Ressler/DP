package com.muni.analyzer.data

final case class ProtocolDefaults(defaultPorts: Set[Int], knownIssue: Boolean)

object ProtocolDefaults {
  def apply(tup: (Set[Int], Boolean)): ProtocolDefaults = {
    ProtocolDefaults(tup._1, tup._2)
  }
}
