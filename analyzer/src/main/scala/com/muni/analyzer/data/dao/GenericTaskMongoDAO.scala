package com.muni.analyzer.data.dao

import monix.eval.Task
import org.mongodb.scala.model.{BulkWriteOptions, Filters}
import org.mongodb.scala.{MongoCollection, MongoDatabase, model}

import scala.language.higherKinds
import scala.reflect.ClassTag

abstract class GenericTaskMongoDAO[T: ClassTag](mongoDB: MongoDatabase,
                                                collectionName: String)
    extends GenericDAO[Task, T] {
  protected val collection: MongoCollection[T] =
    mongoDB.getCollection(collectionName)

  override def insert(document: T): Task[Unit] =
    Task.deferFuture(collection.insertOne(document).toFuture()).map(_ => ())

  override def insertMany(documents: Seq[T]): Task[Unit] =
    Task.deferFuture(collection.insertMany(documents).toFuture()).map(_ => ())

  override def findAll(): Task[Seq[T]] =
    Task.deferFuture(collection.find().toFuture())

  override def findSpecific[Value](key: String, value: Value): Task[Option[T]] =
    Task.deferFuture(collection.find(Filters.eq(key, value)).headOption())

  override def replace[Value](document: T,
                              key: String,
                              value: Value): Task[Unit] = {
    Task
      .deferFuture(
        collection.replaceOne(Filters.eq(key, value), document).toFuture())
      .map(_ => ())
  }

  override def replaceAll[Value](documents: List[T], key: String)(
      filter: T => Value): Task[Unit] = {
    val bulkReplaces = documents.map(device =>
      model.ReplaceOneModel(Filters.eq(key, filter(device)), device))
    Task
      .deferFuture(
        collection
          .bulkWrite(bulkReplaces, BulkWriteOptions().ordered(false))
          .toFuture())
      .map(_ => ())
  }
}
