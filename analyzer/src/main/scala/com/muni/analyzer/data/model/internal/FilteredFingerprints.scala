package com.muni.analyzer.data.model.internal

import com.muni.analyzer.data.model.internal.FilteredFingerprints.SameDevice
import com.muni.analyzer.data.model.json.DeviceFingerprint

final case class FilteredFingerprints(newFingerprints: List[DeviceFingerprint],
                                      toCompareFingerprints: List[SameDevice])

object FilteredFingerprints {
  final case class SameDevice(oldFingerprint: DeviceFingerprint,
                              newFingerprint: DeviceFingerprint)
}
