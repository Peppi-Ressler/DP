package com.muni.analyzer.data.model.internal

import java.io.File

final case class ApplicationArguments(outputFile: Option[File] = None,
                                      inputFile: File = new File("."))
