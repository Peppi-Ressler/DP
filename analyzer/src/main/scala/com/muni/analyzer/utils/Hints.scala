package com.muni.analyzer.utils

import pureconfig.{CamelCase, ConfigFieldMapping, ProductHint}

object Hints {
  implicit def hint[T]: ProductHint[T] =
    ProductHint[T](ConfigFieldMapping(CamelCase, CamelCase))
}
