package com.muni.analyzer.utils

import com.muni.analyzer.data.model.internal.MedianStats

import scala.language.higherKinds

object StatisticHelpers {
  def calculateMean[A: Numeric](numbers: A*): A =
    implicitly[Numeric[A]] match {
      case num: Fractional[A] =>
        import num._
        numbers.sum / fromInt(numbers.length)
      case num: Integral[A] =>
        import num._
        numbers.sum / fromInt(numbers.length)
      case _ => throw new RuntimeException("Undivisable numeric!")
    }

  def calculateCollectionMean[A: Numeric, Repr[X] <: TraversableOnce[X]](
      input: Repr[A]): A = {
    // float, double has different impl of division than int, short, etc.
    implicitly[Numeric[A]] match {
      case num: Fractional[A] =>
        import num._
        input.sum / fromInt(input.size)
      case num: Integral[A] =>
        import num._
        input.sum / fromInt(input.size)
      case _ => throw new RuntimeException("Undivisable numeric!")
    }
  }

  def calculateZScore(input: Double, medianStats: MedianStats): Double = {
    if (medianStats.mad == 0) 0.0
    else {
      val result =
        Math.abs(0.6745 * (input - medianStats.median)) / medianStats.mad
      result
    }
  }

  def getMedianStats(xs: List[Double]): MedianStats = {
    // Extract the median of a sorted, non-empty list of numbers
    def getMedian(nums: List[Double]): Double = {
      val len = nums.length
      if (len % 2 == 0) {
        val (low, high) = nums.splitAt(len / 2)
        (low.last + high.head) / 2
      } else {
        nums(len / 2)
      }
    }

    if (xs.isEmpty) {
      MedianStats(0.0, 0.0)
    } else {
      val nums = xs.sorted
      val median = getMedian(nums)
      val residuals = nums.map(x => Math.abs(x - median)).sorted
      val mad = getMedian(residuals)

      MedianStats(median, mad)
    }
  }
}
