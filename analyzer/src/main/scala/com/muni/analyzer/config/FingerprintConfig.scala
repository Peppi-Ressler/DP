package com.muni.analyzer.config

import com.muni.analyzer.config.FingerprintConfig.ProtocolConfig

final case class FingerprintConfig(protocol: ProtocolConfig)

object FingerprintConfig {
  final case class ProtocolConfig(maxSize: Int)
}
