package com.muni.analyzer.config

final case class DetectionConfig(zScoreThreshold: Float,
                                 similarityThreshold: Float)
