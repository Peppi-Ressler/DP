package com.muni.analyzer.config

import com.muni.analyzer.config.ExecutorsConfig.{Blocking, Callback}

final case class ExecutorsConfig(blocking: Blocking, callback: Callback)

object ExecutorsConfig {
  final case class Blocking(maxSize: Int)
  final case class Callback(maxSize: Int)
}
