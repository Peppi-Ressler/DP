package com.muni.analyzer.config

final case class MongoDBConfig(uri: String)
