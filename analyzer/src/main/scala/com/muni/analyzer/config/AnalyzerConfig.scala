package com.muni.analyzer.config

final case class AnalyzerConfig(executors: ExecutorsConfig,
                                fingerprint: FingerprintConfig,
                                detection: DetectionConfig,
                                mongoDB: MongoDBConfig)
