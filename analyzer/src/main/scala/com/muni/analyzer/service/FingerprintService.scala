package com.muni.analyzer.service

import com.muni.analyzer.data.model.csv.NetflowCsv
import com.muni.analyzer.data.model.internal.FilteredFingerprints.SameDevice
import com.muni.analyzer.data.model.json.DeviceFingerprint

import scala.language.higherKinds

/**
  * Interface for fingerprints creation and update
  * @tparam F effect type
  */
trait FingerprintService[F[_]] {

  /**
    * Method for create fingerprints from netFlows
    * @param netflows
    * @return
    */
  def createFingerprints(
      netflows: Vector[NetflowCsv]): F[Vector[DeviceFingerprint]]

  /**
    * Method for updating fingerprints
    * @param sameDevices
    * @return
    */
  def updateFingerPrints(
      sameDevices: List[SameDevice]): F[List[DeviceFingerprint]]
}
