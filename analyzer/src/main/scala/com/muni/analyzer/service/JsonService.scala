package com.muni.analyzer.service

import io.circe.Encoder

import scala.language.higherKinds

/**
  * Interface for json manipulation
  * @tparam F type of effect
  */
trait JsonService[F[_]] {

  /**
    * Encodes object T into json string
    * @param input
    * @tparam T
    * @return
    */
  def encode[T: Encoder](input: T): F[String]
}
