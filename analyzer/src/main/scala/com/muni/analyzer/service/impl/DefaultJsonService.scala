package com.muni.analyzer.service.impl

import cats.Monad
import com.muni.analyzer.service.JsonService
import com.typesafe.scalalogging.StrictLogging
import io.circe.parser.decode
import io.circe.syntax._
import io.circe.{Decoder, Encoder, Printer}

import scala.language.higherKinds

class DefaultJsonService[F[_]: Monad]
    extends JsonService[F]
    with StrictLogging {
  private[this] val printer = Printer.spaces2.copy(dropNullValues = true)

  override def encode[T: Encoder](input: T): F[String] = {
    Monad[F].pure(input.asJson.pretty(printer))
  }
}
