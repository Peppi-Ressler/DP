package com.muni.analyzer.service

import java.io.File

import purecsv.unsafe.converter.RawFieldsConverter

import scala.language.higherKinds

/**
  * Interface for manipulation with csv files
  * @tparam F type effect
  */
trait CsvService[F[_]] {

  /**
    * Method for transforming information from csv file into objects
    * @param delimeter
    * @param file
    * @tparam T
    * @return
    */
  def readFromFile[T: RawFieldsConverter](delimeter: Char = ',')(
      file: File): F[Vector[T]]

  /**
    * Method for transforming information from csv represented as string into objects
    * @param input
    * @param delimeter
    * @tparam T
    * @return
    */
  def readFromString[T: RawFieldsConverter](input: String,
                                            delimeter: Char = ','): F[Vector[T]]
}
