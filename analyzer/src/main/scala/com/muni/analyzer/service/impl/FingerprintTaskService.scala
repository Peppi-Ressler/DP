package com.muni.analyzer.service.impl

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import cats.syntax.parallel._
import com.muni.analyzer.config.FingerprintConfig
import com.muni.analyzer.data.ProtocolDefaults
import com.muni.analyzer.data.model.csv.{NetflowCsv, ServicePortCsv}
import com.muni.analyzer.data.model.internal.DefaultServiceAndPort
import com.muni.analyzer.data.model.internal.FilteredFingerprints.SameDevice
import com.muni.analyzer.data.model.json.NetFlowStatistics.FlowStats
import com.muni.analyzer.data.model.json._
import com.muni.analyzer.service.{FingerprintService, GeoIPService}
import com.muni.analyzer.utils.StatisticHelpers._
import com.softwaremill.quicklens._
import com.typesafe.scalalogging.StrictLogging
import monix.eval.Task

import scala.annotation.tailrec
import scala.collection.TraversableLike
import scala.collection.generic.CanBuildFrom
import scala.language.higherKinds
import scala.util.Try

class FingerprintTaskService(geoIPService: GeoIPService[Task],
                             servicePortCsv: Vector[ServicePortCsv],
                             protocolUsageConfig: FingerprintConfig)
    extends FingerprintService[Task]
    with StrictLogging {

  // [this] ensures that under the hood direct field access is used instead of virtual method
  private[this] val IP_RANGES = {
    Vector("192.168.", "10.") ++ (16 to 31).map(number => s"172.$number.")
  }
  private[this] val TransportProtocols = Set("TCP", "UDP")
  private[this] val DatePattern1 =
    DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
  private[this] val DatePattern2 =
    DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSSSSS")

  override def createFingerprints(
      netflows: Vector[NetflowCsv]): Task[Vector[DeviceFingerprint]] = {
    val sortedNetflows = netflows.sortBy(_.start_time)
    for {
      defaultPortsAndServices <- getDefaultPortsAndServices
      _ <- Task.delay(logger.info("Default ports parsed"))
      statisticsPerDevice <- createDeviceStatistics(sortedNetflows,
                                                    defaultPortsAndServices)
      _ <- Task.delay(logger.info("Device stats finished"))
      uniqueDomainsPerDevice <- Task(
        createUniqueDomainsPerDevice(statisticsPerDevice))
      _ <- Task.delay(logger.info("Unique domains finished"))
      finalStatistics <- Task(
        createDeviceFingerprint(statisticsPerDevice, uniqueDomainsPerDevice))
      _ <- Task.delay(logger.info("Final stats finished"))
    } yield finalStatistics
  }

  private def createDeviceFingerprint(
      statisticsPerDevice: Vector[((String, String),
                                   Map[String, ProtocolStatistics])],
      uniqueDomains: Map[String, Set[String]]) = {
    statisticsPerDevice
      .map(
        device =>
          DeviceFingerprint(device._1._2,
                            device._1._1,
                            device._2,
                            uniqueDomains.getOrElse(device._1._1, Set.empty)))
  }

  private def createUniqueDomainsPerDevice(
      statisticsPerDevice: Vector[((String, String),
                                   Map[String, ProtocolStatistics])]) = {
    statisticsPerDevice
      .flatMap(
        device =>
          device._2
            .flatMap(_._2.domainStatistics)
            .map(domain => device._1._1 -> domain.domainIp))
      .groupBy(_._2)
      .mapValues(_.map(_._1).toSet)
      .filter(_._2.size == 1)
      .foldLeft(Map.empty[String, Set[String]]) {
        case (result, domains) =>
          domains._2.flatMap {
            case device if result.keySet.contains(device) =>
              val uniqueDomains = result.getOrElse(device, Set.empty) + domains._1
              result + (device -> uniqueDomains)
            case device => result + (device -> Set(domains._1))
          }.toMap
      }
  }

  private def createDeviceStatistics(netflows: Vector[NetflowCsv],
                                     services: List[DefaultServiceAndPort]) = {
    def createProtocolStatistics(device: (String, Vector[NetflowCsv])) = {
      val tasks = device._2
        .groupBy(_.protocol)
        .map {
          case (protocolName: String, netflows: Vector[NetflowCsv]) =>
            val defaults = getDefaultsForProtocol(protocolName, services)
            val protocolUsage = getProtocolUsage(netflows)
            prepareProtocolStatistics(netflows, defaults)
              .map(stats =>
                protocolName -> ProtocolStatistics(stats, protocolUsage))
        }
      Task
        .gatherUnordered(tasks)
        .map((device._1, device._2.head.srcIp) -> _.toMap)
    }
    scanForKnownServices(netflows, services)
      .flatMap(
        flows =>
          Task
            .gatherUnordered(
              flows
                .filter(d => IP_RANGES.exists(d.srcIp.startsWith))
                .groupBy(flow => flow.macAddress)
                .map(createProtocolStatistics))
            .map(_.toVector))
  }

  private def scanForKnownServices(netflows: Vector[NetflowCsv],
                                   services: List[DefaultServiceAndPort]) = {
    val tasks = netflows.map { flow =>
      Task {
        services
          .find(_.defaultPorts.contains(flow.dstPort))
          .map {
            case service
                if !TransportProtocols.contains(flow.protocol) || service.serviceName
                  .equalsIgnoreCase(flow.protocol) || !service.transportProtocols
                  .contains(flow.protocol) =>
              flow
            case service => flow.copy(protocol = service.serviceName)
          }
          .getOrElse(flow)
      }
    }
    Task.gather(tasks)
  }

  private def prepareProtocolStatistics(netflows: Vector[NetflowCsv],
                                        defaults: ProtocolDefaults) = {
    def prepareDomainStatistics(domain: (String, Vector[NetflowCsv])) = {
      val stats =
        calculateStatistics((Vector.empty[FlowStats], Set.empty[Int]),
                            domain._2) { (result, netflow) =>
          (result._1 :+ (netflow.flowSize, netflow.flowDuration, netflow.packets, netflow.flows),
           result._2 + netflow.dstPort)
        }

      (Task(sumFlowStatistics(stats._1)),
       Task(filterDefaultPorts(stats._2, defaults.defaultPorts)),
       geoIPService.getCountry(domain._1)).parMapN {
        (stats, defaults, country) =>
          DomainStatistics(domain._1, stats, defaults, country)
      }
    }

    val protocolStatistics =
      Task(calculateStatistics(Vector.empty[FlowStats], netflows) {
        (result, netflow) =>
          result :+ (netflow.flowSize, netflow.flowDuration, netflow.packets, netflow.flows)
      })
    val domainStatistics =
      Task
        .gatherUnordered(
          netflows
            .groupBy(_.resolvedIp)
            .map(prepareDomainStatistics)
            .sliding(100, 100)
            .toVector
            .map(Task.gather(_)))
        .map(_.flatten)

    // execute in parallel
    (domainStatistics, protocolStatistics).parMapN {
      (domainStats, protocolStats) =>
        (defaults.defaultPorts,
         sumFlowStatistics(protocolStats),
         defaults.knownIssue,
         domainStats)
    }
  }

  private def chooseRightDateFormat(date: String): String => LocalDateTime = {
    def parseDefaultFormat: String => LocalDateTime = { rawDate =>
      LocalDateTime.parse(rawDate)
    }
    def parseCustomFormat(
        pattern: DateTimeFormatter): String => LocalDateTime = { rawDate =>
      LocalDateTime.parse(rawDate, pattern)
    }
    date match {
      case raw if Try(parseDefaultFormat(raw)).isSuccess => parseDefaultFormat
      case raw if Try(parseCustomFormat(DatePattern1)(raw)).isSuccess =>
        parseCustomFormat(DatePattern1)
      case _ => parseCustomFormat(DatePattern2)
    }
  }

  private def getProtocolUsage(netflows: Vector[NetflowCsv]): ProtocolUsage = {
    val dateFunction = chooseRightDateFormat(netflows.head.start_time)

    @tailrec
    def recCount(netflow: NetflowCsv,
                 tail: List[NetflowCsv],
                 maxDate: LocalDateTime,
                 hitsPerMinute: List[Int],
                 averageHitsPerMinute: Int): List[Int] = {
      tail match {
        case h :: t if dateFunction(netflow.start_time).isBefore(maxDate) =>
          recCount(h, t, maxDate, hitsPerMinute, averageHitsPerMinute + 1)
        case h :: t =>
          recCount(h,
                   t,
                   dateFunction(netflow.start_time).plusSeconds(60),
                   hitsPerMinute :+ averageHitsPerMinute,
                   1)
        case _ if hitsPerMinute.isEmpty =>
          hitsPerMinute :+ 1
        case _ =>
          hitsPerMinute :+ averageHitsPerMinute + 1
      }
    }
    val firstFlow = netflows.head
    val minuteHits = recCount(
      firstFlow,
      netflows.tail.to,
      dateFunction(firstFlow.start_time).plusSeconds(60),
      List.empty,
      0)
    ProtocolUsage(calculateCollectionMean(minuteHits),
                  minuteHits.min,
                  minuteHits.max,
                  minuteHits.map(_.toDouble))
  }

  private def getDefaultsForProtocol(
      protocol: String,
      defaultServices: List[DefaultServiceAndPort]) = {
    ProtocolDefaults(
      defaultServices
        .find(_.serviceName.equalsIgnoreCase(protocol))
        .map(r => r.defaultPorts -> r.knownIssue)
        .getOrElse(Set.empty, false))
  }

  private def filterDefaultPorts(ports: Set[Int], defaultPorts: Set[Int]) = {
    ports.diff(defaultPorts)
  }

  private def getDefaultPortsAndServices = {
    Task.gatherUnordered(servicePortCsv
      .groupBy(_.serviceName)
      .map { service =>
        Task {
          val (issue, ports, protocols) =
            service._2.foldLeft((false, Set.empty[Int], Set.empty[String])) {
              case (result, serviceCsv) if serviceCsv.port.contains("-") =>
                val knownIssue = serviceCsv.knownUnauthorizedAccess.nonEmpty
                val ports = {
                  val split = serviceCsv.port.split('-')
                  split(0).toInt to split(1).toInt
                }.toSet
                result.copy(knownIssue,
                            result._2 ++ ports,
                            result._3 + serviceCsv.protocol.toUpperCase)
              case (result, serviceCsv) =>
                val knownIssue = serviceCsv.knownUnauthorizedAccess.nonEmpty
                result.copy(knownIssue,
                            result._2 + serviceCsv.port.toInt,
                            result._3 + serviceCsv.protocol.toUpperCase)
            }
          DefaultServiceAndPort(service._1.toUpperCase, ports, issue, protocols)
        }
      })
  }

  private def calculateStatistics[T, F](res: T, netflows: Vector[F])(
      f: (T, F) => T): T = netflows.foldLeft(res)(f)

  private def sumFlowStatistics(
      tuples: Vector[FlowStats]): NetFlowStatistics = {
    NetFlowStatistics(tuples.foldLeft(0l, 0f, 0, 0) { (result, tuple) =>
      (result._1 + tuple._1,
       result._2 + tuple._2,
       result._3 + tuple._3,
       result._4 + tuple._4)
    })
  }

  override def updateFingerPrints(
      sameDevices: List[SameDevice]): Task[List[DeviceFingerprint]] = {
    Task
      .gatherUnordered(
        sameDevices
          .map { device =>
            Task(
              device.oldFingerprint
                .modify(_.uniqueDomains)
                .using(_ ++ device.newFingerprint.uniqueDomains)
                .modify(_.protocolStatistics)
                .using(addOrUpdateMapElement(
                  _,
                  device.newFingerprint.protocolStatistics) { (x, y) =>
                  x.modify(_.domainStatistics)
                    .using(addOrUpdateIterableElements(_, y.domainStatistics)(
                      (x1, y1) => {
                        x1.modify(_.nonDefaultDstPorts)
                          .using(_ ++ y1.nonDefaultDstPorts)
                          .modify(_.netFlowStatistics)
                          .using(p =>
                            updateFlowStatistics(p, y1.netFlowStatistics))
                      },
                      q => q.domainIp -> q
                    ))
                    .modify(_.protocolUsage)
                    .using(updateProtocolUsage(_, y.protocolUsage))
                    .modify(_.flowStatistics)
                    .using(updateFlowStatistics(_, y.flowStatistics))
                }))
          })
  }

  private def addOrUpdateMapElement[Key, Value](oldMap: Map[Key, Value],
                                                newMap: Map[Key, Value])(
      updateF: (Value, Value) => Value): Map[Key, Value] = {
    newMap.map(
      pair =>
        oldMap
          .get(pair._1)
          .map(pair._1 -> updateF(_, pair._2))
          .getOrElse(pair))
  }

  private def updateFlowStatistics(
      old: NetFlowStatistics,
      update: NetFlowStatistics): NetFlowStatistics = {
    NetFlowStatistics(
      calculateMean(old.flowSize, update.flowSize),
      calculateMean(old.flowDuration, update.flowDuration),
      calculateMean(old.packets, update.packets),
      calculateMean(old.flows, update.flows)
    )
  }

  private def updateProtocolUsage(old: ProtocolUsage,
                                  update: ProtocolUsage): ProtocolUsage = {
    val overallHitsPerMinute = update.hitsPerMinute ++ old.hitsPerMinute
    ProtocolUsage(
      calculateCollectionMean(overallHitsPerMinute),
      overallHitsPerMinute.min.toFloat,
      overallHitsPerMinute.max.toFloat,
      // take first n
      overallHitsPerMinute.slice(0, protocolUsageConfig.protocol.maxSize)
    )
  }

  private def addOrUpdateIterableElements[
      A,
      Repr[X] <: TraversableLike[X, Repr[X]]](oldS: Repr[A], newS: Repr[A])(
      updateF: (A, A) => A,
      tupleF: A => (String, A))(
      implicit cbf: CanBuildFrom[Repr[A], A, Repr[A]]): Repr[A] = {
    addOrUpdateMapElement(oldS.map(tupleF).toMap, newS.map(tupleF).toMap)(
      updateF).values.to
  }
}
