package com.muni.analyzer.service.impl

import cats.effect.Async
import cats.syntax.all._
import com.muni.analyzer.service.GeoIPService
import com.snowplowanalytics.maxmind.iplookups.IpLookups
import com.typesafe.scalalogging.StrictLogging

import scala.language.higherKinds

class DefaultGeoIPService[F[_]: Async](ipLookups: IpLookups)
    extends GeoIPService[F]
    with StrictLogging {
  override def getCountry(address: String): F[String] = {
    Async[F].async(
      _(
        ipLookups
          .performLookups(address)
          .ipLocation
          .flatMap(_.toOption)
          .collect {
            case result if result.countryName != null => result.countryName
          }
          .getOrElse("unknown")
          .asRight))
  }
}
