package com.muni.analyzer.service

import com.muni.analyzer.data.model.internal.FilteredFingerprints.SameDevice
import com.muni.analyzer.data.model.json.FinalResult

import scala.language.higherKinds

/**
  * Interface for finding anomalies
  * @tparam F type effect
  */
trait DetectionService[F[_]] {

  /**
    * Method for finding anomaly among fingerprints
    * @param toCompareFingerprints
    * @return
    */
  def findAnomaly(
      toCompareFingerprints: List[SameDevice]): F[Option[List[FinalResult]]]
}
