package com.muni.analyzer.service.impl

import cats.data.{NonEmptyList, OptionT}
import cats.instances.double._
import cats.syntax.all._
import com.muni.analyzer.config.DetectionConfig
import com.muni.analyzer.data.model.internal.FilteredFingerprints.SameDevice
import com.muni.analyzer.data.model.internal.{Anomaly, Malware}
import com.muni.analyzer.data.model.json.FinalResult.ProtocolReport
import com.muni.analyzer.data.model.json.{
  DeviceFingerprint,
  FinalResult,
  ProtocolStatistics
}
import com.muni.analyzer.service.DetectionService
import com.muni.analyzer.utils.StatisticHelpers._
import com.typesafe.scalalogging.StrictLogging
import monix.eval.Task

import scala.collection.TraversableLike
import scala.language.higherKinds

class DetectionTaskService(malware: Malware, config: DetectionConfig)
    extends DetectionService[Task]
    with StrictLogging {
  private[this] val UnknownName = "unknown"

  override def findAnomaly(toCompareFingerprints: List[SameDevice])
    : Task[Option[List[FinalResult]]] = {
    val maybeResult = for {
      relationship <- OptionT.liftF(
        findRelationsBetweenDevices(toCompareFingerprints))
      result = evaluateAnalysis(relationship)
      if result.nonEmpty
      finalReport <- OptionT.liftF(prepareOutput(result))
    } yield finalReport
    maybeResult.value
  }

  private def evaluateAnalysis(analysis: List[Anomaly]) = {
    analysis.filter { rs =>
      rs.similarity < config.similarityThreshold ||
      rs.zScore.exists(_._2 >= config.zScoreThreshold) ||
      rs.botnets.nonEmpty
    }
  }

  private def prepareOutput(anomalies: List[Anomaly]) = {
    def toProtocolReport(protocolStatistics: ProtocolStatistics) = {
      ProtocolReport(protocolStatistics.defaultPorts,
                     protocolStatistics.flowStatistics,
                     protocolStatistics.misused,
                     protocolStatistics.domainStatistics.toList)
    }

    Task.gatherUnordered(anomalies.map { anomaly =>
      val newProtocols =
        Task(
          anomaly.infected.protocolStatistics
            .filterNot(protocol =>
              anomaly.original.protocolStatistics.contains(protocol._1))
            .map {
              case (protocolName, stats) =>
                protocolName -> toProtocolReport(stats)
            })
      val oldSuspicious = Task(
        anomaly.zScore
          .filter(_._2 >= config.zScoreThreshold)
          .map {
            case (domain, _) =>
              domain -> anomaly.infected.protocolStatistics.get(domain)
          }
          .collect {
            case (protocol, Some(statistics)) => protocol -> statistics
          }
          .map {
            case (protocolName, stats) =>
              protocolName -> toProtocolReport(stats)
          })
      val malwareNames = Task(
        NonEmptyList
          .fromList(anomaly.botnets.filter(_._2 != UnknownName).values.toList)
          .map(_.toList.toSet)
          .getOrElse(Set(UnknownName)))
      (newProtocols, oldSuspicious, malwareNames).parMapN {
        (newP, oldP, mNames) =>
          FinalResult(
            anomaly.original.macAddress,
            newP,
            oldP,
            mNames,
            anomaly.botnets.keys.toSet
          )
      }
    })
  }

  private def blacklistsCheckout(deviceFingerprint: DeviceFingerprint) = {
    Task(
      deviceFingerprint.protocolStatistics.flatMap(
        _._2.domainStatistics
          .map(ds => ds.domainIp -> malware.botnets.get(ds.domainIp))
          .collect {
            case (ip, Some(r)) => ip -> r
          }
          .toMap))
  }

  private def findRelationsBetweenDevices(devices: List[SameDevice]) = {

    Task
      .gatherUnordered(devices.map { device =>
        val protocolsHits =
          detectProtocols(device.newFingerprint, device.oldFingerprint)
        val domainsHits =
          detectDomains(device.newFingerprint, device.oldFingerprint)
        val blackListed = blacklistsCheckout(device.newFingerprint)
        (domainsHits, protocolsHits, blackListed).parMapN {
          (domains, protocols, black) =>
            val values = domains + protocols._1
            Anomaly(device.oldFingerprint,
                    device.newFingerprint,
                    values / 2,
                    protocols._2.toMap,
                    black)
        }
      })
  }

  private def detectProtocols(update: DeviceFingerprint,
                              old: DeviceFingerprint) = {
    def checkProtocolsZScore = {
      Task
        .gatherUnordered(old.protocolStatistics.map {
          t =>
            update.protocolStatistics
              .get(t._1)
              .map { up =>
                val values =
                  t._2.protocolUsage.hitsPerMinute ++ up.protocolUsage.hitsPerMinute
                val filteredZScore = NonEmptyList
                  .fromList(
                    up.protocolUsage.hitsPerMinute // we want values outside white range
                      .filter(num =>
                        num < t._2.protocolUsage.lowerBoundary || num > t._2.protocolUsage.higherBoundary)
                      .map(calculateZScore(_, getMedianStats(values))))
                  .map(_.maximum)
                Task.now(t._1 -> filteredZScore.getOrElse(0.0))
              }
              .getOrElse(Task.now(t._1 -> 0.0))
        })
    }

    val sumFields = Task(
      sumFieldsWeights(update.protocolStatistics, old.protocolStatistics)(_._1))

    (sumFields, checkProtocolsZScore).parMapN { (sum, zScore) =>
      sum -> zScore
    }
  }

  private def detectDomains(update: DeviceFingerprint,
                            old: DeviceFingerprint) = {
    def zipOnProtocol(update: Map[String, ProtocolStatistics],
                      old: Map[String, ProtocolStatistics]) = {
      (update.toList ++ old.toList)
        .groupBy(_._1)
        .mapValues(_.map(_._2))
    }

    Task {
      val perProtocol =
        zipOnProtocol(update.protocolStatistics, old.protocolStatistics)
          .foldLeft(List.empty[Float]) {
            case (result, stats) if stats._2.tail.isEmpty =>
              result :+ 0f
            case (result, stats) =>
              // always just two
              result :+ sumFieldsWeights(
                stats._2.head.domainStatistics,
                stats._2.last.domainStatistics)(_.domainIp)
          }
      calculateCollectionMean(perProtocol)
    }

  }

  private def sumFieldsWeights[A, Repr[X] <: TraversableLike[X, Repr[X]], B](
      collection: Repr[A],
      oldCollection: Repr[A])(mapF: A => B) = {
    val collectionValues = collection.map(mapF)
    val oldCollectionValues = oldCollection.map(mapF)
    val updatedValues = collectionValues.toSet
    val oldValues = oldCollectionValues.toSet

    val intersect = updatedValues.intersect(oldValues).size.toFloat
    val union = updatedValues.union(oldValues).size.toFloat
    intersect / union
  }
}
