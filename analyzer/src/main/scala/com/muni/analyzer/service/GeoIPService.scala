package com.muni.analyzer.service

import scala.language.higherKinds

/**
  * Interface for geoIp manipulation
  * @tparam F type of effect
  */
trait GeoIPService[F[_]] {

  /**
    * method for obtaining country based on ip address or domain name
    * @param address
    * @return
    */
  def getCountry(address: String): F[String]
}
