package com.muni.analyzer.service.impl

import java.io.File

import cats.effect.Async
import com.muni.analyzer.service.CsvService
import com.typesafe.scalalogging.StrictLogging
import purecsv.unsafe.CSVReader
import purecsv.unsafe.converter.RawFieldsConverter

import scala.language.higherKinds
import scala.util.Try

class DefaultCsvService[F[_]: Async] extends CsvService[F] with StrictLogging {
  override def readFromFile[T: RawFieldsConverter](delimeter: Char)(
      file: File): F[Vector[T]] = {
    Async[F].async(
      _(
        Try(CSVReader[T]
          .readCSVFromFile(file, skipHeader = true, delimeter))
          .map(_.toVector)
          .toEither))
  }

  override def readFromString[T: RawFieldsConverter](
      input: String,
      delimeter: Char): F[Vector[T]] = {
    Async[F].async(
      _(
        Try(CSVReader[T].readCSVFromString(input, skipHeader = true, delimeter))
          .map(_.toVector)
          .toEither))
  }
}
