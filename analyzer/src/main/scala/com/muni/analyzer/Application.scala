package com.muni.analyzer

import java.io.File

import com.muni.analyzer.config.AnalyzerConfig
import com.muni.analyzer.data.model.internal.ApplicationArguments
import com.muni.analyzer.setup._
import com.muni.analyzer.utils.Hints._
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.StrictLogging
import resource._

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

object Application extends App with StrictLogging {
  withArgsParser(args) { parsedArgs =>
    withConfiguration { (config, rawConfig) =>
      logger.info("Analyzer is starting")
      for {
        executors <- managed(new ExecutorModule(config.executors))
        mongoDB <- managed(
          new MongoDBModule(config.mongoDB)(executors.scheduler))
        services <- Services(executors,
                             mongoDB,
                             config.fingerprint,
                             config.detection)
      } {
        import executors._

        val result =
          services.fingerprintFacade
            .processFingerprint(parsedArgs.inputFile, parsedArgs.outputFile)
            .onErrorHandle(ex =>
              logger.error(s"Internal error occurred: ${ex.getMessage}"))
            .runAsync
        Await.result(result, 5 minutes)
      }
    }
  }

  private def withConfiguration(run: (AnalyzerConfig, Config) => Unit): Unit = {
    val config = ConfigFactory.load()

    pureconfig.loadConfig[AnalyzerConfig](config) match {
      case Right(skyControlConfig) => run(skyControlConfig, config)

      case Left(errors) =>
        errors.toList.foreach { failure =>
          logger.error(s"${failure.description}")
        }
        System.exit(-1)
    }
  }

  private def withArgsParser(args: Array[String])(
      run: ApplicationArguments => Unit): Unit = {
    createArgsParser.parse(args, ApplicationArguments()) match {
      case Some(parsedArgs) => run(parsedArgs)
      case None             => System.exit(-1)
    }
  }

  private def createArgsParser = {
    new scopt.OptionParser[ApplicationArguments]("analyzer") {
      head("Anomaly detection system")

      opt[File]('o', "out")
        .optional()
        .text("When specified output will be saved into <output> file")
        .valueName("output")
        .action((f, r) => r.copy(outputFile = Some(f)))

      opt[File]('i', "input")
        .valueName("file")
        .required()
        .validate(i =>
          if (i.exists() && i.canRead) success
          else
            failure(
              s"Input file ${i.getName} does not exists or application has not enough permissions!"))
        .action((i, args) => args.copy(inputFile = i))
        .text("Required csv file")

      note("By default output is printed on stdout")
    }
  }
}
