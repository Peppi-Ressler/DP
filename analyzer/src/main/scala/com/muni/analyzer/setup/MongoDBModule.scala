package com.muni.analyzer.setup

import com.muni.analyzer.config.MongoDBConfig
import com.muni.analyzer.data.dao.{FingerprintRepository, GenericDAO}
import com.muni.analyzer.data.entity.{
  DeviceFingerprintEntity,
  DomainStatisticsEntity,
  ProtocolStatisticsEntity
}
import com.muni.analyzer.data.model.json.{NetFlowStatistics, ProtocolUsage}
import com.typesafe.scalalogging.StrictLogging
import monix.eval.Task
import monix.execution.Scheduler
import org.bson.codecs.configuration.CodecRegistries.{
  fromProviders,
  fromRegistries
}
import org.mongodb.scala.MongoClient
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

class MongoDBModule(mongoDbConfig: MongoDBConfig)(implicit scheduler: Scheduler)
    extends AutoCloseable
    with StrictLogging {
  private[this] val codecRegistry =
    fromRegistries(
      fromProviders(classOf[DeviceFingerprintEntity],
                    classOf[ProtocolStatisticsEntity],
                    classOf[NetFlowStatistics],
                    classOf[DomainStatisticsEntity],
                    classOf[ProtocolUsage]),
      DEFAULT_CODEC_REGISTRY
    )
  private[this] val mongoClient = MongoClient(mongoDbConfig.uri)
  private[this] val database =
    mongoClient.getDatabase("local").withCodecRegistry(codecRegistry)

  private[this] val collections = List("fingerprints")

  Await.result(prepareDbScheme().runAsync, 15 seconds)

  val fingerprintDao: GenericDAO[Task, DeviceFingerprintEntity] =
    new FingerprintRepository(database, "fingerprints")

  private def prepareDbScheme() = {
    for {
      existingCollections <- Task.deferFuture(
        database.listCollectionNames.toFuture)
      _ <- Task.gatherUnordered(createMissingCollections(existingCollections))
    } yield ()
  }

  private def createMissingCollections(existingCollections: Seq[String]) = {
    collections
      .filterNot(existingCollections.contains)
      .map(collection =>
        Task.deferFuture(database.createCollection(collection).toFuture()))
  }

  override def close(): Unit = mongoClient.close()
}
