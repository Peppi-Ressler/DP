package com.muni.analyzer

import cats.arrow.FunctionK
import cats.{Applicative, Monad, Parallel, ~>}
import monix.eval.Task
import monix.eval.instances.CatsParallelForTask

package object setup {
  implicit final val parallelTask: Parallel[Task, Task] =
    new Parallel[Task, Task] {
      val applicative: Applicative[Task] = new Applicative[Task] {
        private[this] val applicative = CatsParallelForTask.applicative
        override def pure[A](x: A): Task[A] = Task.pure(x)

        override def ap[A, B](ff: Task[A => B])(fa: Task[A]): Task[B] = {
          Task.Par.unwrap(applicative.ap(Task.Par(ff))(Task.Par(fa)))
        }
      }

      val monad: Monad[Task] = CatsParallelForTask.monad

      val sequential: Task ~> Task = FunctionK.id

      val parallel: Task ~> Task = FunctionK.id
    }
}
