package com.muni.analyzer.setup

import java.io._
import java.nio.file.{Files, Paths, StandardCopyOption}
import java.util.jar.JarFile

import cats.instances.list._
import cats.syntax.all._
import com.muni.analyzer.config.{DetectionConfig, FingerprintConfig}
import com.muni.analyzer.data.model.csv.ServicePortCsv
import com.muni.analyzer.data.model.internal.Malware
import com.muni.analyzer.facade.DefaultFingerprintFacade
import com.muni.analyzer.service._
import com.muni.analyzer.service.impl._
import com.muni.analyzer.setup.Services.InitialDataF
import com.snowplowanalytics.maxmind.iplookups.IpLookups
import monix.eval.Task
import purecsv.unsafe.converter.RawFieldsConverter
import resource._

import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.{higherKinds, postfixOps}

class Services(executorModule: ExecutorModule,
               mongoDBModule: MongoDBModule,
               protocolUsageConfig: FingerprintConfig,
               detectionConfig: DetectionConfig)(initDataF: InitialDataF) {

  import executorModule.scheduler

  val jsonParser: JsonService[Task] = new DefaultJsonService

  val csvService: CsvService[Task] = new DefaultCsvService

  private[this] val (defaultPorts, blacklist, geoIpPath) =
    Await.result(initDataF(csvService).runAsync, 10 seconds)

  val geoIPService: GeoIPService[Task] = new DefaultGeoIPService(
    IpLookups(geoFile = Some(geoIpPath)))

  val fingerprintService: FingerprintService[Task] =
    new FingerprintTaskService(geoIPService, defaultPorts, protocolUsageConfig)

  val detectionService: DetectionService[Task] =
    new DetectionTaskService(blacklist, detectionConfig)

  val fingerprintFacade: DefaultFingerprintFacade[Task] =
    new DefaultFingerprintFacade(csvService,
                                 fingerprintService,
                                 jsonParser,
                                 detectionService,
                                 mongoDBModule.fingerprintDao)
}

object Services {
  private final case class InitData(
      initFunction: CsvService[Task] => Task[
        (Vector[ServicePortCsv], Malware, String)])

  private[this] val TemporaryGeoIpPath = "GeoLite2-City.mmdb"

  implicit private val rawString: RawFieldsConverter[String] =
    new RawFieldsConverter[String] {

      override def to(a: String): Seq[String] = List(a)

      override def from(b: Seq[String]): String = b.head
    }
  implicit private val rawTuple: RawFieldsConverter[(String, String)] =
    new RawFieldsConverter[(String, String)] {

      override def to(a: (String, String)): Seq[String] = List(a._1, a._2)

      override def from(b: Seq[String]): (String, String) =
        b.head -> b.tail.head
    }
  private type InitialDataF =
    CsvService[Task] => Task[(Vector[ServicePortCsv], Malware, String)]
  private implicit val deleteOnExit: Resource[InitData] = _ =>
    Files.deleteIfExists(Paths.get(TemporaryGeoIpPath))

  private def getInitData: InitialDataF = { csvService =>
    val path = "blacklist"
    val jarFile = new File(
      getClass.getProtectionDomain.getCodeSource.getLocation.getPath)

    if (jarFile.isFile) { // Run with JAR file
      val jar = new JarFile(jarFile)
      val entries = Task
        .gatherUnordered(
          jar.entries.asScala
            .filter(_.getName.startsWith(s"$path/"))
            .map(f => new File(s"$path/${f.getName}"))
            .filter(_.isFile)
            .map(csvService.readFromFile[(String, String)]()))
        .map(_.flatten.toMap)
      val servicePorts = inputStreamToString(
        getClass.getResourceAsStream("/ports_v3.csv"))
      val ports =
        csvService.readFromString[ServicePortCsv](servicePorts, ';')

      inputStreamToBinaryFile(
        getClass.getResourceAsStream("/GeoLite2-City.mmdb"),
        TemporaryGeoIpPath)
      val geoIpDb = new File(TemporaryGeoIpPath).getAbsolutePath
      val result = (entries, ports).parMapN { (entr, por) =>
        (por, Malware(entr), geoIpDb)
      }
      // need to close file
      result.bracket(Task.now)(_ => Task.delay(jar.close()))
    } else {
      val resource = getClass.getResource("/blacklist")
      val folder = new File(resource.getPath)
      val allBlacklists = folder.listFiles.toList
        .map(csvService.readFromFile[(String, String)]())
        .parSequence
        .map(_.flatten.toMap)
      val portsFile = inputStreamToString(
        getClass.getResourceAsStream("/ports_v3.csv"))
      val geoIpDb = getClass.getResource("/GeoLite2-City.mmdb").getPath
      val servicePorts =
        csvService.readFromString[ServicePortCsv](portsFile, ';')
      (allBlacklists, servicePorts).parMapN { (blacklists, ports) =>
        (ports, Malware(blacklists), geoIpDb)
      }
    }
  }

  private def inputStreamToBinaryFile(is: InputStream,
                                      outputFileName: String) = {
    val result = for {
      br <- managed(is)
    } yield {
      Files.copy(br,
                 Paths.get(outputFileName),
                 StandardCopyOption.REPLACE_EXISTING)
    }
    result.acquireAndGet(r => r)
  }

  private def inputStreamToString(is: InputStream) = {
    // stream decorators wrap its close method over original close method of input stream
    val result = for {
      isr <- managed(new InputStreamReader(is))
      br <- managed(new BufferedReader(isr))
    } yield {
      Iterator continually br.readLine takeWhile (_ != null) mkString "\n"
    }
    result.acquireAndGet(r => r)
  }

  def apply(executorModule: ExecutorModule,
            mongoDBModule: MongoDBModule,
            protocolUsageConfig: FingerprintConfig,
            detectionConfig: DetectionConfig): ManagedResource[Services] = {
    for {
      initData <- managed(InitData(getInitData))
    } yield
      new Services(executorModule,
                   mongoDBModule,
                   protocolUsageConfig,
                   detectionConfig)(initData.initFunction)
  }
}
