package com.muni.analyzer.setup

import java.util.concurrent.{
  ForkJoinPool,
  SynchronousQueue,
  ThreadPoolExecutor,
  TimeUnit
}

import com.muni.analyzer.config.ExecutorsConfig
import monix.execution.Scheduler

import scala.concurrent.{ExecutionContext, ExecutionContextExecutorService}

class ExecutorModule(executorsConfig: ExecutorsConfig) extends AutoCloseable {

  private[this] val threadPool: ForkJoinPool = new ForkJoinPool(
    executorsConfig.callback.maxSize)

  implicit val scheduler: Scheduler = Scheduler(threadPool)

  val blockingExecutor: ExecutionContextExecutorService =
    ExecutionContext.fromExecutorService {
      new ThreadPoolExecutor(0,
                             executorsConfig.blocking.maxSize,
                             60L,
                             TimeUnit.SECONDS,
                             new SynchronousQueue[Runnable])
    }

  override def close(): Unit = {
    threadPool.shutdown()
    blockingExecutor.shutdown()
  }
}
