package com.muni.analyzer.facade

import java.io.File

import scala.language.higherKinds

/**
  * Interface for main business logic. Facade which creates fingerprints from netFlows and eventually search for anomalies
  * @tparam F type effect
  */
trait FingerprintFacade[F[_]] {

  /**
    * Method which creates fingerprints and searches for anomalies
    * @param inputFile
    * @param outputFile
    * @return
    */
  def processFingerprint(inputFile: File, outputFile: Option[File]): F[Unit]
}
