package com.muni.analyzer.facade

/**
  * Type class interface for transforming json models into DAO objects
  * @tparam Json
  * @tparam DAO
  */
trait JsonToDAO[Json, DAO] {

  /**
    * Method for transformation from Json object to DAO
    * @param input
    * @param id
    * @return
    */
  def toDao(input: Json, id: Int): DAO
}
