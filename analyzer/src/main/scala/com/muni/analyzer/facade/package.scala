package com.muni.analyzer

import cats.Parallel
import com.muni.analyzer.data.entity.DeviceFingerprintEntity
import com.muni.analyzer.data.model.json._

import scala.language.higherKinds

package object facade {
  implicit class JsonDAO[Model](models: Seq[(Model, Int)]) {
    def jsonToDao[T](implicit transform: JsonToDAO[Model, T]): Seq[T] = {
      models.map(t => transform.toDao(t._1, t._2))
    }
  }

  implicit class DeviceStatisticsToDAO(from: List[DeviceFingerprint]) {
    def toDAO: List[DeviceFingerprintEntity] = {
      from.map(DeviceFingerprintEntity.apply)
    }
  }

  implicit class fromDaoToDeviceStatistics(from: Seq[DeviceFingerprintEntity]) {
    def fromDAO: Seq[DeviceFingerprint] =
      from.map(DeviceFingerprint.apply).to
  }

  type DefaultParallel[F[_]] = Parallel[F, F]
}
