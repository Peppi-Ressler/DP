package com.muni.analyzer.facade

import java.io.File
import java.nio.charset.StandardCharsets
import java.nio.file.Files

import cats.data.NonEmptyList
import cats.syntax.all._
import cats.{Monad, Parallel}
import com.muni.analyzer.data._
import com.muni.analyzer.data.dao.GenericDAO
import com.muni.analyzer.data.entity.DeviceFingerprintEntity
import com.muni.analyzer.data.model.csv.NetflowCsv
import com.muni.analyzer.data.model.internal.FilteredFingerprints
import com.muni.analyzer.data.model.internal.FilteredFingerprints.SameDevice
import com.muni.analyzer.data.model.json.{DeviceFingerprint, FinalResult}
import com.muni.analyzer.service._
import com.typesafe.scalalogging.StrictLogging

import scala.language.higherKinds

class DefaultFingerprintFacade[F[_]: Monad: DefaultParallel](
    csvService: CsvService[F],
    fingerprintService: FingerprintService[F],
    jsonService: JsonService[F],
    detectionService: DetectionService[F],
    fingerprintDAO: GenericDAO[F, DeviceFingerprintEntity])
    extends StrictLogging
    with FingerprintFacade[F] {

  private[this] val MacAddressKey = "macAddress"

  def processFingerprint(inputFile: File, outputFile: Option[File]): F[Unit] = {
    for {
      _ <- Monad[F].pure(logger.info("Analyzer started"))
      netflows <- csvService.readFromFile[NetflowCsv]()(inputFile)
      _ = logger.info("Input data successfully loaded")
      fingerprints <- fingerprintService.createFingerprints(netflows)
      _ = logger.info("fingerprint completed")
      existingFingerprints <- findAllByMacAddress(fingerprints)
      filteredFingerprints <- filterExistingFingerprints(existingFingerprints,
                                                         fingerprints)
      _ <- detectionService
        .findAnomaly(filteredFingerprints.toCompareFingerprints)
        .flatMap(processDetectionResults(outputFile, filteredFingerprints))
    } yield ()
  }

  private def updateAndStore(filteredFingerprints: FilteredFingerprints) = {
    for {
      _ <- storeIfNonEmpty(filteredFingerprints.newFingerprints.toDAO.to)
      updated <- fingerprintService
        .updateFingerPrints(filteredFingerprints.toCompareFingerprints)
        .map(NonEmptyList.fromList)
      _ <- updated.fold(Monad[F].pure(()))(
        nonEmptyList =>
          fingerprintDAO.replaceAll(nonEmptyList.toList.toDAO, MacAddressKey)(
            _.macAddress))
    } yield ()
  }

  private def processDetectionResults(
      outputFile: Option[File],
      filteredFingerprints: FilteredFingerprints)
    : Option[List[FinalResult]] => F[Unit] = {
    case Some(anomaly) =>
      jsonService
        .encode(anomaly)
        .map(json =>
          outputFile.map(saveOutputToFile(json, _)).getOrElse(println(json)))
    case None =>
      updateAndStore(filteredFingerprints)
        .map(_ => println("No anomaly found"))
  }

  private def findAllByMacAddress(
      fingerPrints: Vector[DeviceFingerprint]): F[Vector[DeviceFingerprint]] = {
    import cats.instances.vector._

    Parallel
      .parSequence[Vector, F, F, Option[DeviceFingerprintEntity]](
        fingerPrints.map(fp =>
          fingerprintDAO.findSpecific(MacAddressKey, fp.macAddress)))
      .map(_.collect {
        case Some(value) => DeviceFingerprint(value)
      })
  }

  private def saveOutputToFile(json: String, outputFile: File): Unit = {
    // internally handles also closing source
    Files.write(outputFile.toPath, json.getBytes(StandardCharsets.UTF_8))
    ()
  }

  // store new devices
  private def storeIfNonEmpty(
      newFingerprints: Vector[DeviceFingerprintEntity]): F[Unit] =
    if (newFingerprints.nonEmpty) fingerprintDAO.insertMany(newFingerprints)
    else Monad[F].pure(Unit)

  private def filterExistingFingerprints(
      oldFingerprints: Vector[DeviceFingerprint],
      fingerprints: Vector[DeviceFingerprint]) = {
    val existing = fingerprints.foldLeft(
      (List.empty[DeviceFingerprint], List.empty[SameDevice])) { (result, fp) =>
      oldFingerprints.find(_.macAddress == fp.macAddress) match {
        case Some(value) => result._1 -> (result._2 :+ SameDevice(value, fp))
        case None        => (result._1 :+ fp) -> result._2
      }
    }
    Monad[F].pure(FilteredFingerprints(existing._1, existing._2))
  }
}
