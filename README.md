#Anomaly detector
##About
This project has two modules. **pcapParser.py** transforms pcap files into csv files containing netFlows. **Analyzer** takes csv files as input and creates fingerprint based on information obtained from netFlows. Then it saves fingerprint into db or creates anomaly report.
## technology
 - Scala 2.12
 - MongoDB 4.0.3
 - Python 3.6
    - tshark
 

## How to run this project
 - Download and run mongoDB
 - Put mongoDB uri into reference.conf (like uri = "mongodb://localhost:27017") of analyzer module
 - Make sure that tshark is installed
 - Run **python pcapParser.py -p [absolute path to pcap file]**
    - Optionally there could be used -t flag which indicates timeout between flows. Default is 10 seconds
 - In same directory as pcapParser csv file with netFlows will be created
 - Run **java -jar analyzator.jar -i [absolute path to csv file]**
    - All logs are stored in analyzer.log which is created in same directory as **.jar** file.
    - In case of no anomaly detected there will be printed "no anomaly" on stdout else anomaly report would be printed on stdout
    - With optional flag **-o** the output report can be saved into file based on provided absolute path   
   