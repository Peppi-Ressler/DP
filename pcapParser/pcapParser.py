from __future__ import print_function, division
from subprocess import check_call
import argparse
import os


def parse_records_tshark(f_name):
    records = []
    names = ['start_time', 'src_ip', 'dst_ip', 'protocol', 'length',
            'src_port', 'dst_port', 'date', 'resolved_dst', 'mac']
    with open(f_name, 'r') as infile:
        for line in infile:
            line = line.strip()
            items = line.split()
            try:
                rec = (float(items[1]), items[2], items[4], items[5], items[6],
                       int(items[7]), int(items[8]), items[9] + " " + items[10], items[11], items[12])
                records.append(rec)
            except:
                pass
    os.remove(f_name)
    return records, names


def export_to_txt(f_name, txt_f_name):
    cmd = """tshark -o column.format:'"No.", "%%m", "Time", "%%t", "Source", "%%us", "Destination", "%%ud", "Protocol", "%%p", "len", "%%L", "srcport", "%%uS", "dstport", "%%uD", "StartTime", "%%Yut", "resolvedSource", "%%rd", "mac", "%%uhs"' -r %s > %s""" % (f_name, txt_f_name)

    print('--> ', cmd)
    check_call(cmd, shell=True)

    """
%q	802.1Q VLAN id
%Yt	Absolute date, as YYYY-MM-DD, and time
%YDOYt	Absolute date, as YYYY/DOY, and time
%At	Absolute time
%V	Cisco VSAN
%B	Cumulative Bytes
%Cus	Custom
%y	DCE/RPC call (cn_call_id / dg_seqnum)
%Tt	Delta time
%Gt	Delta time displayed
%rd	Dest addr (resolved)
%ud	Dest addr (unresolved)
%rD	Dest port (resolved)
%uD	Dest port (unresolved)
%d	Destination address
%D	Destination port
%a	Expert Info Severity
%I	FW-1 monitor if/direction
%F	Frequency/Channel
%hd	Hardware dest addr
%hs	Hardware src addr
%rhd	Hw dest addr (resolved)
%uhd	Hw dest addr (unresolved)
%rhs	Hw src addr (resolved)
%uhs	Hw src addr (unresolved)
%e	IEEE 802.11 RSSI
%x	IEEE 802.11 TX rate
%f	IP DSCP Value
%i	Information
%rnd	Net dest addr (resolved)
%und	Net dest addr (unresolved)
%rns	Net src addr (resolved)
%uns	Net src addr (unresolved)
%nd	Network dest addr
%ns	Network src addr
%m	Number
%L	Packet length (bytes)
%p	Protocol
%Rt	Relative time
%s	Source address
%S	Source port
%rs	Src addr (resolved)
%us	Src addr (unresolved)
%rS	Src port (resolved)
%uS	Src port (unresolved)
%E	TEI
%Yut	UTC date, as YYYY-MM-DD, and time
%YDOYut	UTC date, as YYYY/DOY, and time
%Aut	UTC time
%t	Time (format as specified)
"""


def change_to_flows(records, name, time_out):
    t_seq = name.index('start_time')
    length_seq = name.index('length')
    date_start_index = name.index('date')
    resolved_dst_index = name.index('resolved_dst')
    mac_index = name.index('mac')
    five_tuple_seq = [name.index(k) for k in ['protocol', 'src_ip', 'src_port', 'dst_ip', 'dst_port']]
    open_flows = dict()
    res_flow = []
    for rec in records:
        five_tuple = tuple(rec[seq] for seq in five_tuple_seq)
        packet_start_time = rec[t_seq]
        length = int(rec[length_seq])
        date_start = rec[date_start_index]
        resolved_dst = rec[resolved_dst_index]
        new_mac = rec[mac_index]

        stored_rec = open_flows.get(five_tuple, None)
        if stored_rec is not None:
            (st_time_old, last_time_old, length_old, packetCount, date, flows, resDst, mac) = stored_rec
            # delay between packets too long -> add another flow
            if packet_start_time - last_time_old > time_out or last_time_old - packet_start_time > time_out:
                open_flows[five_tuple] = (st_time_old, packet_start_time, length_old + length, packetCount + 1, date, flows + 1, resDst, mac)
            else:
                open_flows[five_tuple] = (st_time_old, packet_start_time, length_old + length, packetCount + 1, date, flows, resDst, mac)
        else:
            open_flows[five_tuple] = (packet_start_time, packet_start_time, length, 1, date_start, 1, resolved_dst, new_mac)

    for f_tuple, (flowStartTime, last_time, length, packetCount, date, flows, resDst, mac) in open_flows.items():
        # format: date_flow_start duration protocol src_ip port dst_ip port packets bytes flows resolvedDstIp macAddress
        res_flow.append((date, ) + (last_time - flowStartTime, ) + f_tuple + (packetCount, ) + (length, ) + (flows, ) + (resDst, ) + (mac, ))
    print("""
Total Packets: [%i]
Exported Flows: [%i]
            """ % (len(records), len(res_flow)))

    return res_flow


def write_flow(flows, f_name):
    fid = open(f_name, 'w')
    for f in flows:
        fid.write(','.join([str(v) for v in f]) + '\n')
    fid.close()


def pcap2flow(pcap_file_name, time_out):
    txt_f_name = pcap_file_name.rsplit('.pcap')[0] + '_tshark.txt'
    export_to_txt(pcap_file_name, txt_f_name)
    records, name = parse_records_tshark(txt_f_name)
    res_flows = change_to_flows(records, name, time_out)
    write_flow(res_flows, pcap_file_name.rsplit('.pcap')[0] + '_flow.csv')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='pcap analysis')
    parser.add_argument('-p', '--pcap', default=None, help='specify the pcap file you want to process')
    parser.add_argument('-t', '--timeout', default=10.0, help='specify timeout between same flows')

    args = parser.parse_args()
    if args.pcap:
        pcap2flow(args.pcap, args.timeout)
    else:
        print("NO ARGS PROVIDED!")
        exit(-1)
